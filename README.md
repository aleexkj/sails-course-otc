# Introducción a Sails.js

Proyecto base del curso en OTC - ENP6.

## Requisitos
- node v8.11.3
- En config/local.js agrega tus credenciales de Pusher, siguiendo config/custom.js

## Instalación

- nvm use 8.11.3
- npm install

## Ejecución

- npm start

## Colaboradores

- Alejandra Colaoapa
    - aleexkj@ciencias.unam.mx
    - aleexkj@gitlab
- Carlos Campos
    - carlos.campos@enp.unam.mx
    - carlos.22.12.95@gitlab
