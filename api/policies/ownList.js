module.exports = function(req, res, next) {
  List.findOne({
    user: req.session.userId,
    id: req.params.id
  }).exec((err, result) => {
    if (err) {
      return res.serverError(err);
    }

    if (!result) {
      return res.forbidden('Not your list!');
    }

    req.options.name = result.name;
    next();
  })
};