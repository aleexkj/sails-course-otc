const Pusher = require('pusher');

module.exports = function(sails) {
  return {
    defaults: {
      custom: {
        pusher: {}
      }
    },

    configure: function(){
      if (!sails.config.custom.pusher.appId) {
        sails.config.custom.pusher.appId = 'hola';
      }
    },

    initialize: function (cb) {
      const pusher = new Pusher(sails.config.custom.pusher);

      global.pusher = pusher;
      cb();
    }
  };
};