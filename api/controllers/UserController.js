/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const bcrypt = require('bcrypt');

module.exports = {
  create: async function(req, res){
    if(req.body.pass1 == req.body.pass2) {
      try {
        const user = await User.create({email: req.body.email, password: req.body.pass1}).fetch();

        req.session.userId = user.id;
        return res.redirect('/lists');
      } catch (error) {
        console.log(error);
        return res.view('pages/homepage', {logupError: "No se pudo crear"});
      }
    }
    return res.view('pages/homepage', {logupError:"Contraseñas no coinciden"});
  },

  login:function (req, res) {
    User.findOne({email: req.body.email}, (error, user)=>{
      bcrypt.compare(req.body.password, user.password, (error, result)=>{
        if(result){
          req.session.userId = user.id;
          return res.redirect('/lists');
        }else{
          return res.view('pages/homepage',{loginError: "Verifica tus datos"})
        }
      });
    });
  },

  logout: function(req, res) {
    req.session.userId = undefined;
    res.redirect('/');
  }
};

