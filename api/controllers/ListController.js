/**
 * CategoryController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
module.exports = {
  create: async function(req, res){
	  const list = await List.create({
      name: req.body.name,
      user: req.session.userId
	  }).fetch();
	  return res.json(list);
  },

  getAll: function(req, res) {
    User.findOne({
      id: req.session.userId
    })
      .populate('lists')
      .populate('sharedLists')
      .exec((err, user) => {
        if (err) {
          return res.serverError(err);
        }

        res.view('pages/lists', {
          lists: user.lists,
          sharedLists: user.sharedLists,
          channel: 'shared-lists-' + req.session.userId
        });
      });
  },

  destroy: async function(req, res){
		await List.destroy({id:req.params.id});
		return res.json("Eluminado");
  },

  share: function(req, res) {
    let userId;

    async.waterfall([
      (cb) => {
        User.findOne({
          email: req.body.email
        }).exec(cb)
      },
      (user, cb) => {
        userId = user.id;
        User.addToCollection(user.id, 'sharedLists', req.params.id)
          .exec(cb);
      }
    ], (err) => {
      if (err) {
        return res.serverError(err);
      }

      pusher.trigger('shared-lists-' + userId, 'new-list', {
        id: req.params.id,
        name: req.options.name
      });
      res.ok();
    });
  }
};

