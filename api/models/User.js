/**
 * User.js
 *
 */
const bcrypt = require('bcrypt');

module.exports = {
  attributes: {
    email: {
      type: 'string',
      required: true,
      isEmail: true
    },

    password: {
      type: 'string',
      required: true
    },

    lists: {
      collection: 'list',
      via: 'user'
    },

    sharedLists: {
      collection: 'list',
      via: 'sharedWith'
    }
  },
  beforeCreate : function(data, proceed){
    bcrypt.hash(data.password, 3, (err, hash)=>{
      data.password = hash;
      return proceed();
    });
  }
};

